import java.util.*;


/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */
    public void run() {
        Graph g = new Graph("G");

        // Create Graph with positive weights.
        g.createSimpleWeightedGraph(2500, 1000);
        System.out.println(g);

        long stime = new Date().getTime();
        Graph result = g.kruskalMinAlg();
        long ftime = new Date().getTime();
        int diff = Long.valueOf(ftime - stime).intValue();
      //  System.out.println(result);
        System.out.println(diff);
    }


    /**
     * Implement Kruskal's algorithm assuming that the edge (arc in my case) weights are positive integers.
     * Find a minimum weight set of arcs that connect all the vertices without cycles.
     */


    /**
     * Vertex class
     */
    class Vertex {
        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }

    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {
        private String id;
        private Vertex source;
        private Vertex target;
        private Arc next;
        private int weight = 0;

        Arc(String s, Vertex vs, Vertex vt, Arc a) {
            id = s;
            source = vs;
            target = vt;
            next = a;
        }

        Arc(String s) {
            this(s, null, null, null);
        }

        @Override
        public String toString() {
            return id + " w" + weight;
        }

        public int getWeight() {
            return this.weight;
        }

        public void setWeight(int v) {
            weight = v;
        }
    }


    class Graph {
        private String id;
        private Vertex first;
        private int info = 0;

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            res.source = from;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n, int maxWeight) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    int rnd = (int) (Math.random() * maxWeight + 1);
                    Arc a = createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    a.setWeight(rnd);
                    Arc b = createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                    b.setWeight(rnd);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Method to create undirected Graph with positive integer weighted Arcs
         *
         * @param n         number of Arcs
         * @param maxWeight range of weight
         */
        public void createSimpleWeightedGraph(int n, int maxWeight) {
            if (n <= 0) {
                return;
            }
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            first = null;
            createRandomTree(n, maxWeight);
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (i == j) {
                        continue;
                    }
                    if (connected[i][j] != 0 || connected[j][i] != 0) {
                        continue;
                    }
                    Vertex vi = vert[i];
                    Vertex vj = vert[j];
                    int rnd = (int) (Math.random() * maxWeight) + 1;
                    Arc a = createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                    a.setWeight(rnd);
                    connected[i][j] = 1;
                    Arc b = createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                    b.setWeight(rnd);
                    connected[j][i] = 1;
                }
            }
        }

        /**
         * Subset class to represent Vertex as set with id.
         */
        class Subset {
            int id;
            private Vertex vertex;

            Subset(int id, Vertex vertex) {
                this.id = id;
                this.vertex = vertex;
            }
        }

        /**
         * Method to find id of subset with given Vertex
         *
         * @param subsets Array of sets
         * @param v       Vertex to compare
         * @return id of subset.
         */
        public int find(Subset[] subsets, Vertex v) {
            for (Subset set : subsets) {
                if (set.vertex == v) {
                    return set.id;
                }
            }
            throw new RuntimeException("Vertex " + v.toString() + " is not correct.");
        }

        /**
         * Method to connect subsets by given them same id.
         *
         * @param subsets Array of sets
         * @param x       id of first subset
         * @param y       id of second subset
         */
        public void union(Subset[] subsets, int x, int y) {
            for (Subset subset : subsets) {
                if (subset.id == y) {
                    subset.id = x;
                }
            }
        }

        /**
         * Given connected graph
         *
         * @return Graph with minimum weight Arc without cycles
         */
        public Graph kruskalMinAlg() {
            Graph resultGraph = new Graph("minSpanGraph");
            List<Arc> arcList = new ArrayList<>();
            List<Vertex> vertexList = new ArrayList<>();

            // Sort Arcs to remove reverse way direction
            Vertex v = first;
            while (v != null) {
                Arc a = v.first;
                while (a != null) {
                    if (!vertexList.contains(a.target)) {
                        arcList.add(a);
                    }
                    a = a.next;
                }
                vertexList.add(v);
                v = v.next;
            }

            //sort arcs based on their weight ascending
            Arc[] arcs = arcList.stream().sorted(Comparator.comparing(Arc::getWeight)).toArray(Arc[]::new);

            // Create  subset array with single elements and different id.
            Subset[] subsets = new Subset[vertexList.size()];
            int vertCount = 0;
            for (Vertex ver : vertexList) {
                Subset a = new Subset(vertCount, ver);
                subsets[vertCount] = a;
                vertCount++;
            }

            //Create minimum weighted arc array without cycles.
            Arc[] resultArcList = new Arc[vertexList.size() - 1];
            vertCount = 0;
            int count = 0;
            while (count < vertexList.size() - 1) {
                Arc nextArc = arcs[vertCount++];
                int x = find(subsets, nextArc.source);  // search of set id to which belong vertex
                int y = find(subsets, nextArc.target);  //
                if (x != y) {                           // checking for cycle
                    resultArcList[count++] = nextArc;
                    union(subsets, x, y);               // connecting sets
                }
            }

            // From this line this code is only for make result Graph to be undirected
            // because of arc based architecture of main Graph class.

            //Making arcs back with reverse direction, to represent undirected graph.
            Arc[] extended = new Arc[resultArcList.length * 2]; //extend arc array for reversed direction arcs
            System.arraycopy(resultArcList, 0, extended, 0, resultArcList.length);

            int index = resultArcList.length;
            for (Arc a : resultArcList) {
                StringBuilder stB = new StringBuilder();
                stB.append("a");
                stB.append(a.target);
                stB.append("_");
                stB.append(a.source);
                Arc temp = new Arc(stB.toString());
                temp.source = a.target;
                temp.target = a.source;
                temp.setWeight(a.getWeight());
                extended[index] = temp;
                index++;
            }

            //Adding vertexes to result graph
            Collections.reverse(vertexList);
            for (Vertex vert : vertexList) {
                Vertex newVert = new Vertex(vert.id);
                newVert.next = resultGraph.first;
                resultGraph.first = newVert;
            }
            //Connecting minimum weighted arcs
            Vertex next = resultGraph.first;
            while (next != null) {
                for (Arc a : extended) {
                    if (a.source.id == next.id) {
                        a.next = next.first;
                        next.first = a;
                    }
                }
                next = next.next;
            }
            return resultGraph;
        }
    }
}
